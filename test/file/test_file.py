#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestFile(FelixTestCase):

    def test_file(self):
        self.assertFile("test/file/test_file.py", "test/file/test_file.py")


if __name__ == '__main__':
    unittest.main()
