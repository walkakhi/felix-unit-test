#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestUUID(FelixTestCase):

    def test_uuid(self):
        uuid1 = FelixTestCase.get_uuid()
        uuid2 = FelixTestCase.get_port()
        uuid3 = FelixTestCase.get_port()
        self.assertNotEqual(uuid1, uuid2)
        self.assertNotEqual(uuid2, uuid3)
        self.assertNotEqual(uuid1, uuid3)

    def test_uuid_class(self):
        self.assertIsNotNone(FelixTestCase.uuid)
        self.assertIsNotNone(FelixTestCase.env.get('UUID'))
        self.assertEqual(FelixTestCase.uuid, FelixTestCase.env.get('UUID'))


if __name__ == '__main__':
    unittest.main()
