#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestFile(FelixTestCase):

    def test_random_fid_class(self):
        self.assertIsNotNone(FelixTestCase.did)
        self.assertIsNotNone(FelixTestCase.cid)
        self.assertIsNotNone(FelixTestCase.elink)
        self.assertIsNotNone(FelixTestCase.alt_elink)
        self.assertIsNotNone(FelixTestCase.link)
        self.assertIsNotNone(FelixTestCase.alt_link)
        self.assertIsNotNone(FelixTestCase.fid)
        self.assertIsNotNone(FelixTestCase.fid_10)
        self.assertIsNotNone(FelixTestCase.fid_sid)
        self.assertIsNotNone(FelixTestCase.fid_alt_sid)
        self.assertIsNotNone(FelixTestCase.alt_fid)
        self.assertIsNotNone(FelixTestCase.alt_fid_sid)
        self.assertIsNotNone(FelixTestCase.alt_fid_alt_sid)
        self.assertIsNotNone(FelixTestCase.fid_fm)
        self.assertIsNotNone(FelixTestCase.fid_sid_fm)
        self.assertIsNotNone(FelixTestCase.fid_alt_sid_fm)
        self.assertIsNotNone(FelixTestCase.alt_fid_fm)
        self.assertIsNotNone(FelixTestCase.alt_fid_sid_fm)
        self.assertIsNotNone(FelixTestCase.alt_fid_alt_sid_fm)
        self.assertIsNotNone(FelixTestCase.fid_toflx)
        self.assertIsNotNone(FelixTestCase.alt_fid_toflx)

    def test_random_fid_env(self):
        self.assertIsNotNone(FelixTestCase.env.get('DID'))
        self.assertIsNotNone(FelixTestCase.env.get('CID'))
        self.assertIsNotNone(FelixTestCase.env.get('ELINK'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_ELINK'))
        self.assertIsNotNone(FelixTestCase.env.get('LINK'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_LINK'))
        self.assertIsNotNone(FelixTestCase.env.get('FID'))
        self.assertIsNotNone(FelixTestCase.env.get('FID_10'))
        self.assertIsNotNone(FelixTestCase.env.get('FID_SID'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_FID'))
        self.assertIsNotNone(FelixTestCase.env.get('FID_ALT_SID'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_FID_SID'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_FID_ALT_SID'))
        self.assertIsNotNone(FelixTestCase.env.get('FID_FM'))
        self.assertIsNotNone(FelixTestCase.env.get('FID_SID_FM'))
        self.assertIsNotNone(FelixTestCase.env.get('FID_ALT_SID_FM'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_FID_FM'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_FID_SID_FM'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_FID_ALT_SID_FM'))
        self.assertIsNotNone(FelixTestCase.env.get('FID_TOFLX'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_FID_TOFLX'))

        self.assertEqual(FelixTestCase.did, FelixTestCase.env.get('DID'))
        self.assertEqual(FelixTestCase.cid, FelixTestCase.env.get('CID'))
        self.assertEqual(FelixTestCase.elink, FelixTestCase.env.get('ELINK'))
        self.assertEqual(FelixTestCase.alt_elink, FelixTestCase.env.get('ALT_ELINK'))
        self.assertEqual(FelixTestCase.link, FelixTestCase.env.get('LINK'))
        self.assertEqual(FelixTestCase.alt_link, FelixTestCase.env.get('ALT_LINK'))
        self.assertEqual(FelixTestCase.fid, FelixTestCase.env.get('FID'))
        self.assertEqual(FelixTestCase.fid_10, FelixTestCase.env.get('FID_10'))
        self.assertEqual(FelixTestCase.fid_sid, FelixTestCase.env.get('FID_SID'))
        self.assertEqual(FelixTestCase.fid_alt_sid, FelixTestCase.env.get('FID_ALT_SID'))
        self.assertEqual(FelixTestCase.alt_fid, FelixTestCase.env.get('ALT_FID'))
        self.assertEqual(FelixTestCase.alt_fid_sid, FelixTestCase.env.get('ALT_FID_SID'))
        self.assertEqual(FelixTestCase.alt_fid_alt_sid, FelixTestCase.env.get('ALT_FID_ALT_SID'))
        self.assertEqual(FelixTestCase.fid_fm, FelixTestCase.env.get('FID_FM'))
        self.assertEqual(FelixTestCase.fid_sid_fm, FelixTestCase.env.get('FID_SID_FM'))
        self.assertEqual(FelixTestCase.fid_alt_sid_fm, FelixTestCase.env.get('FID_ALT_SID_FM'))
        self.assertEqual(FelixTestCase.alt_fid_fm, FelixTestCase.env.get('ALT_FID_FM'))
        self.assertEqual(FelixTestCase.alt_fid_sid_fm, FelixTestCase.env.get('ALT_FID_SID_FM'))
        self.assertEqual(FelixTestCase.alt_fid_alt_sid_fm, FelixTestCase.env.get('ALT_FID_ALT_SID_FM'))
        self.assertEqual(FelixTestCase.fid_toflx, FelixTestCase.env.get('FID_TOFLX'))
        self.assertEqual(FelixTestCase.alt_fid_toflx, FelixTestCase.env.get('ALT_FID_TOFLX'))


if __name__ == '__main__':
    unittest.main()
