#!/usr/bin/env python3

import json
import unittest

from felix_test_case import FelixTestCase


class TestJSON(FelixTestCase):

    def test_json_bus(self):
        with open('test/json/bus1.json') as f:
            bus1 = json.load(f)

        bus2 = [
            {
                "fid": 124,
                "ip": "192.168.100.2",
                "port": 5133,
                "unbuffered": False,
                "pubsub": False,
                "netio_pages": 256,
                "netio_pagesize": 65536
            },
            {
                "ip": "192.168.100.1",
                "fid": 123,
                "port": 5122,
                "unbuffered": False,
                "pubsub": False,
                "netio_pages": 256,
                "netio_pagesize": 65536
            }
        ]

        self.assertJSON(bus1, bus2)


if __name__ == '__main__':
    unittest.main()
