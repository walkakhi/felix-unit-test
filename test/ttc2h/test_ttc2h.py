#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestFile(FelixTestCase):

    def test_ttc2h(self):

        self.assertEqual(FelixTestCase.ttc2h_elink, '0x33b' if int(FelixTestCase.regmap_version, 0) < 0x0500 else '0x600')


if __name__ == '__main__':
    unittest.main()
