#pragma once

#include <stdlib.h>
#include <string.h>

int is_running_in_valgrind ()
{
  char *p = getenv ("LD_PRELOAD");
  if (p == NULL)
    return 0;
  return (strstr (p, "/valgrind/") != NULL ||
          strstr (p, "/vgpreload") != NULL);
}
