cmake_minimum_required(VERSION 3.4.3)

set(PACKAGE felix-unit-test)
set(PACKAGE_VERSION 0.0.1)

include(FELIX)

# others
#felix_add_external(catch ${catch_version} ${BINARY_TAG})

add_library(felix-unit-test INTERFACE)
target_include_directories(felix-unit-test INTERFACE include)

#
# Other Tests
#
add_test(felix-unit-test-fid test/fid/test_fid.py)
add_test(felix-unit-test-file test/file/test_file.py)
add_test(felix-unit-test-iface test/iface/test_iface.py)
add_test(felix-unit-test-json test/json/test_json.py)
add_test(felix-unit-test-port test/port/test_port.py)
add_test(felix-unit-test-regexp test/regexp/test_regexp.py)
add_test(felix-unit-test-supervisord test/supervisord/test_supervisord.py)
add_test(felix-unit-test-supervisord-error test/supervisord-error/test_supervisord_error.py)
add_test(felix-unit-test-ttc2h test/ttc2h/test_ttc2h.py)
add_test(felix-unit-test-uuid test/uuid/test_uuid.py)

add_custom_target(felix-unit-test-python ALL
	COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/python ${CMAKE_CURRENT_BINARY_DIR})
add_custom_target(felix-unit-test-scripts ALL
	COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/scripts ${CMAKE_CURRENT_BINARY_DIR})
add_custom_target(felix-unit-test-tests ALL
	COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/test ${CMAKE_CURRENT_BINARY_DIR}/test)
